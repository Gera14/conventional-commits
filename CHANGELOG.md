# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2022-01-24)


### Features

* add release feature ([d9fc929](https://gitlab.com/Gera14/conventional-commits/commit/d9fc929b8caf4fc016faa60f77a44bd01b0f2cfe))
* create app ([2ab9365](https://gitlab.com/Gera14/conventional-commits/commit/2ab9365cbae770304b9b1e10e0c98f2733a06e7f))


### Bug Fixes

* change console log messages ([022dbfe](https://gitlab.com/Gera14/conventional-commits/commit/022dbfef536f7fdb40ed436226f4c451b1e5879b))
* change readme description ([b431293](https://gitlab.com/Gera14/conventional-commits/commit/b431293ff1f1abddca66c7f1964f4d297d2ae53c))
